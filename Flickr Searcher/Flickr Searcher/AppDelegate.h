//
//  AppDelegate.h
//  Flickr Searcher
//
//  Created by Nate on 3/7/15.
//  Copyright (c) 2015 ncpApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

