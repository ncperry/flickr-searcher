//
//  UIView+Extensions.h
//  Jolt
//
//  Created by Jolt on 10/20/14.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define OVERLAY_VIEW_TAG 4321   // to recognize overlay views

@interface UIView (Extensions)

- (CGFloat)maxX;
- (CGFloat)maxY;
@property( nonatomic, assign) CGFloat x;
@property( nonatomic, assign) CGFloat y;
@property( nonatomic, assign) CGFloat width;
@property( nonatomic, assign) CGFloat height;




- (UIView *)commonSuperview: (UIView *)otherView;

- (void)pinToSuperview: (NSString *)spec inset: (CGFloat)inset;
- (void)pinToView: (UIView *)v1 format: (NSString *)spec offset: (CGFloat)offset;
- (NSArray *)pinToView: (UIView *)v1 spec: (NSString *)fmt, ... NS_FORMAT_FUNCTION( 2, 3);
- (NSArray *)pinToSuperview: (NSString *)fmt, ... NS_FORMAT_FUNCTION( 1, 2);
- (NSArray *)pin: (NSString *)spec, ... NS_FORMAT_FUNCTION( 1, 2);
- (void)addSubview: (UIView *)v pin: (NSString *)spec, ... NS_FORMAT_FUNCTION( 2, 3);


@end
