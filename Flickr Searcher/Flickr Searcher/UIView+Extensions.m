//
//  UIView+Extensions.m
//  Jolt
//
//  Created by Jolt on 10/20/14.
//
//

#import "UIView+Extensions.h"

@implementation UIView (Extensions)

- (CGFloat)maxX
{
    return self.frame.origin.x + self.frame.size.width;
}

- (CGFloat)maxY
{
    return self.frame.origin.y + self.frame.size.height;
}


- (CGFloat)x
{
    return self.frame.origin.x;
}


- (void)setX: (CGFloat)x
{
    CGRect r = self.frame;
    r.origin.x = x;
    self.frame = r;
}


- (CGFloat)y
{
    return self.frame.origin.y;
}

- (void)setY: (CGFloat)y
{
    CGRect r = self.frame;
    r.origin.y = y;
    self.frame = r;
}


- (CGFloat)width
{
    return self.frame.size.width;
}

- (void)setWidth: (CGFloat)width
{
    CGRect r = self.frame;
    r.size.width = width;
    self.frame = r;
}


- (CGFloat)height
{
    return self.frame.size.height;
}

- (void)setHeight: (CGFloat)height
{
    CGRect r = self.frame;
    r.size.height = height;
    self.frame = r;
}


- (UITapGestureRecognizer *)setTapTarget: (id)target selector: (SEL)selector
{
    //
    // Maybe remove existing tap recognizers.
    //
    UITapGestureRecognizer *gr0 = [[UITapGestureRecognizer alloc] initWithTarget: target action: selector];
    [self addGestureRecognizer: gr0];
    self.userInteractionEnabled = YES;
    return gr0;
}


- (UILongPressGestureRecognizer *)setLongPressTarget: (id)target selector: (SEL)selector
{
    UILongPressGestureRecognizer *gr0 = [[UILongPressGestureRecognizer alloc] initWithTarget: target action: selector];
    [self addGestureRecognizer: gr0];
    self.userInteractionEnabled = YES;
    return gr0;
}


- (UIView *)addOverlayViewWithMessage: (NSString *)message
{
    UIView *v = [[UIView alloc] init];
    v.translatesAutoresizingMaskIntoConstraints = NO;
    v.backgroundColor = [UIColor darkGrayColor];
    v.alpha = 0.8;
    v.tag = OVERLAY_VIEW_TAG;
    UILabel *lbl = [[UILabel alloc] init];
    lbl.numberOfLines = 0;
    lbl.lineBreakMode = NSLineBreakByWordWrapping;
    lbl.translatesAutoresizingMaskIntoConstraints = NO;
    lbl.text = message;
    lbl.textColor = [UIColor whiteColor];
    lbl.font = [UIFont fontWithName: @"Arial" size: 30];
    lbl.textAlignment = NSTextAlignmentCenter;
    [self addSubview: v];
    NSDictionary *views = @{ @"overlay" : v};
    [self addConstraints: [NSLayoutConstraint constraintsWithVisualFormat: @"V:|[overlay]|"
                                                                  options: 0 metrics: nil views: views]];
    [self addConstraints: [NSLayoutConstraint constraintsWithVisualFormat: @"|[overlay]|"
                                                                  options: 0 metrics: nil views: views]];
    views = @{ @"label" : lbl, @"superview" : v};
    [v addSubview: lbl];
    [v addConstraint: [NSLayoutConstraint constraintWithItem: lbl
                                                   attribute: NSLayoutAttributeWidth
                                                   relatedBy: NSLayoutRelationLessThanOrEqual
                                                      toItem: v
                                                   attribute: NSLayoutAttributeWidth
                                                  multiplier: 0.75
                                                    constant: 0]];
    
    NSLayoutConstraint *cs = [NSLayoutConstraint constraintWithItem: lbl
                                                          attribute: NSLayoutAttributeCenterY  // NSLayoutAttributeBottom
                                                          relatedBy: NSLayoutRelationEqual
                                                             toItem: v
                                                          attribute: NSLayoutAttributeCenterY
                                                         multiplier: 1.0 constant: 0];
    //    cs.priority = UILayoutPriorityDefaultLow;
    [v addConstraint: cs];
    
    
    [v addConstraint: [NSLayoutConstraint constraintWithItem: lbl
                                                   attribute: NSLayoutAttributeCenterX
                                                   relatedBy: NSLayoutRelationEqual
                                                      toItem: v
                                                   attribute: NSLayoutAttributeCenterX
                                                  multiplier: 1.0 constant: 0]];
    //    [v addConstraints: [NSLayoutConstraint constraintsWithVisualFormat: @"H:[superview]-(<=1)-[label]"
    //                                                               options: NSLayoutFormatAlignAllCenterY
    //                                                               metrics: nil views: views]];
    //    [v addConstraints: [NSLayoutConstraint constraintsWithVisualFormat: @"V:[superview]-(<=1)-[label]"
    //                                                               options: NSLayoutFormatAlignAllCenterX
    //                                                               metrics: nil views: views]];
    return v;
}

- (void)removeOverlayView
{
    UIView *v = [self viewWithTag: OVERLAY_VIEW_TAG];
    [v removeFromSuperview];
}

- (BOOL)hasOverlayView
{
    UIView *v = [self viewWithTag: OVERLAY_VIEW_TAG];
    return v != nil;
}

#pragma mark - Michael's Autolayout Helpers

//
// Michael's autolayout helpers
//


- (UIView *)commonSuperview: (UIView *)otherView
{
    NSMutableSet *views = [NSMutableSet set];
    UIView *view = self;
    
    do {
        if (view != nil) {
            if ([views member:view])
                return view;
            [views addObject:view];
            view = view.superview;
        }
        
        if (otherView != nil) {
            if ([views member:otherView])
                return otherView;
            [views addObject:otherView];
            otherView = otherView.superview;
        }
    } while (view || otherView);
    
    return nil;
}


- (NSLayoutAttribute)layoutAttributeForChar: (char)c
{
    switch (c) {
        case 'l': return NSLayoutAttributeLeft;
        case 'r': return NSLayoutAttributeRight;
        case 't': return NSLayoutAttributeTop;
        case 'b': return NSLayoutAttributeBottom;
        case 'c': return NSLayoutAttributeCenterX;
        case 'd': return NSLayoutAttributeCenterY;
        case 'w': return NSLayoutAttributeWidth;
        case 'h': return NSLayoutAttributeHeight;
    }
    return 0;
}

- (void)pinToSuperview: (NSString *)spec inset: (CGFloat)inset
{
    UIView *v = self.superview;
    [v pinToView: self format: spec offset: inset];
}

- (void)pinToView: (UIView *)v1 format: (NSString *)spec offset: (CGFloat)offset
{
    UIView *v = [self commonSuperview: v1];
    if (!v)
        return;
    NSLayoutConstraint *cs;
    NSLayoutAttribute la0, la1, la;
    NSArray *pairs = [spec componentsSeparatedByString: @","];
    NSMutableArray *c = [NSMutableArray array];
    CGFloat os;
    for (NSString *pair in pairs) {
        int n = 0;
        NSLayoutRelation r = NSLayoutRelationEqual;
        la0 = 0;
        la1 = 0;
        const char *s, *fmt = pair.UTF8String;
        for (s = fmt; *s; s++) {
            switch (*s) {
                case 'l':
                case 'r':
                case 't':
                case 'b':
                case 'c':   // center x
                case 'd':   // center y
                case 'w':   // width
                case 'h':   // height
                    la = [self layoutAttributeForChar: *s];
                    if (n == 0)
                        la0 = la;
                    else if (n == 1)
                        la1 = la;
                    n++;
            }
        }
        if (la1 == 0)
            la1 = la0;
        if (la0 == 0) {
            continue;
        }
        os = offset;
        if (la1 == NSLayoutAttributeLeft || la1 == NSLayoutAttributeTop) {
            os = -os;
#if DEBUG
            if (os && v1.superview != self)
                NSLog( @"");
#endif
        }
        cs = [NSLayoutConstraint constraintWithItem: self
                                          attribute: la0
                                          relatedBy: r
                                             toItem: v1
                                          attribute: la1
                                         multiplier: 1.0
                                           constant: os];
        if (cs)
            [c addObject: cs];
    }
    [v addConstraints: c];
}


- (NSArray *)pinToSuperview: (NSString *)fmt, ...
{
    va_list args;
    va_start( args, fmt);
    NSString *spec = [[NSString alloc] initWithFormat: fmt arguments: args];
    va_end( args);
    
    UIView *v = self.superview;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wformat-nonliteral"
    return [self pinToView: v spec: spec];
#pragma clang diagnostic pop
}

//
// Format string for Michael's autolayout helper
//
// A format is a string of terms separated by commas.
// A term is one or two keys and an optional signed or unsigned decimal number.
//
// For pinToView, the first key refers to self and the second key
// refers to argView.  If there is no second key, the constraint
// applies only to self.
//
// For pinToSuperview, the first key refers to self and
// the second key refers to argView.
//
// If present, the optional decimal number represents the constant of the
// constraint.
//
// Keys:
//
//  l   left side
//  r   right side
//  t   top side
//  b   bottom side
//  c   horizontal center
//  d   vertical center
//  w   width
//  h   height
//
//  >   make the constraint be >= instead of ==
//  <   make the constraint be <= instead of ==
//  @   the next number is a constraint priority
//

- (NSArray *)pinToView: (UIView *)argView spec: (NSString *)fmt, ...
{
    va_list args;
    va_start( args, fmt);
    NSString *spec = [[NSString alloc] initWithFormat: fmt arguments: args];
    va_end( args);
    
    UIView *v0 = argView ? [self commonSuperview: argView] : self;
    if (!v0)
        return @[];
    NSMutableArray *constraints = [NSMutableArray array];
    UIView *v1 = self, *v2 = nil;
    NSLayoutConstraint *cs;
    NSLayoutAttribute la1, la2, la;
    NSArray *terms = [spec componentsSeparatedByString: @","];
    CGFloat constant, f, priority, multiplier, spriority;
    CGFloat *val;
    BOOL neg, dec, mult, inNum;
    
#define CLOSE_NUM() do { \
f = 1.0; \
dec = NO; \
if (neg) { \
*val = -*val; \
neg = NO; \
} \
inNum = NO; \
} while (0)
    
    spriority = UILayoutPriorityRequired;
    for (NSString *term in terms) {
        //
        // Reset default values for each term
        //
        int n = 0;
        val = &constant;
        constant = 0.0;
        multiplier = 1.0;
        priority = spriority;
        f = 1.0;
        neg = NO;
        dec = NO;
        mult = NO;
        inNum = NO;
        NSLayoutRelation r = NSLayoutRelationEqual;
        la1 = 0;
        la2 = 0;
        v2 = nil;
        const char *s, *fmt = term.UTF8String;
        for (s = fmt; *s; s++) {
            switch (*s) {
                case 'l':
                case 'r':
                case 't':
                case 'b':
                case 'c':   // center x
                case 'd':   // center y
                case 'w':   // width
                case 'h':   // height
                    la = [self layoutAttributeForChar: *s];
                    if (n == 0)
                        la1 = la;
                    else if (n == 1) {
                        la2 = la;
                        v2 = argView;
                    }
                    else if (n > 1) {
                        NSLog( @"%s: Bad spec: \"%@\"", __FUNCTION__, spec);
                        return nil;
                    }
                    n++;
                    break;
                    
                case '>':
                    r = NSLayoutRelationGreaterThanOrEqual;
                    break;
                    
                case '<':
                    r = NSLayoutRelationLessThanOrEqual;
                    break;
                    
                case '-':
                    if (inNum) {
                        CLOSE_NUM();
                        val = &constant;
                    }
                    neg = YES;
                    break;
                    
                case '+':
                    if (inNum) {
                        CLOSE_NUM();
                        val = &constant;
                    }
                    break;
                    
                case '*':
                    //
                    // * signifies the start of a multiplier value
                    // so finish up the current value and prepare to
                    // receive the multiplier.
                    //
                    if (inNum) {
                        CLOSE_NUM();
                    }
                    val = &multiplier;
                    break;
                    
                case '@':
                    //
                    // * signifies the start of a priority value
                    // so finish up the current value and prepare to
                    // receive the multiplier.
                    //
                    if (inNum) {
                        CLOSE_NUM();
                    }
                    val = &priority;
                    break;
                    
                case '.':
                    dec = YES;
                    break;
                    
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    if (!inNum) {
                        inNum = YES;
                        *val = 0.0;
                    }
                    if (dec) {
                        f = f / 10.0;
                        *val = *val + f * (*s - '0');
                    }
                    else {
                        *val = *val * 10.0 + (*s) - '0';
                    }
                    break;
            }
        }
        if (inNum)
            CLOSE_NUM();
        if (la2 == 0)
            la2 = la1;
        if (la1 == 0) {
            //
            // If there were no layout attributes in this term, then interpret
            // it as a term to set the other parameters to apply to the other
            // terms in the spec.  This behavior allows for example the priority
            // to be set at the start of the spec and not have to be repeated
            // for every term.  E.g. "@500,w34,ll10,rr-10" would set 3 constraints,
            // all at a priority of 500 (as opposed to having to do this:
            // "w32@500,ll10@500,rr-10@500").
            // May be extended to other keys as well, later.
            //
            spriority = priority;
            continue;
        }
        
        //
        // Sign flipping.  The purpose of this code was to make it so that
        // insets with respect to a containing view could be specified by
        // all positive constants.  However this routine is used for more
        // than insets, and in those cases it confused the issue. I may add
        // a global flag to spec to control the behavior. Until then, leave
        // this out.
        //
#if 0
        if (la2 == NSLayoutAttributeLeft || la2 == NSLayoutAttributeTop) {
            constant = -constant;
#if DEBUG
            if (constant && v2.superview != v1)
                DBLog( @"");
#endif
        }
#endif
        
        cs = [NSLayoutConstraint constraintWithItem: v1
                                          attribute: la1
                                          relatedBy: r
                                             toItem: v2
                                          attribute: la2
                                         multiplier: multiplier
                                           constant: constant];
        if (cs) {
            cs.priority = priority;
            //
            // If the constraint only involves v1, add the constraint to v1.
            // Otherwise add it to v0
            //
            if (!v2 || v1 == v2)
                [v1 addConstraint: cs];
            else
                [v0 addConstraint: cs];
            [constraints addObject: cs];
        }
    }
    return constraints;
}


//
// TODO: Merge this functionality into pinToView and make this routine just
// call that with nil argView.
//
- (NSArray *)pin: (NSString *)fmt, ...
{
    va_list args;
    va_start( args, fmt);
    NSString *spec = [[NSString alloc] initWithFormat: fmt arguments: args];
    va_end( args);
    
    NSMutableArray *constraints = [NSMutableArray array];
    CGFloat v = 0.0, f, mult;
    NSArray *parts = [spec componentsSeparatedByString: @","];
    NSLayoutAttribute la, la2;
    NSLayoutRelation r;
    NSLayoutConstraint *cs;
    UIView *view2 = nil;
    for (NSString *part in parts) {
        const char *s, *p = part.UTF8String;
        BOOL dec = NO;
        la = NSLayoutAttributeNotAnAttribute;
        v = 0.0, f = 1.0, mult = 1.0;
        if (*p == 'w' || *p == 'h' || *p == 'a') {
            if (*p == 'a') {
                //
                // Special case: aspect ratio
                //
                la = NSLayoutAttributeWidth;
                la2 = NSLayoutAttributeHeight;
                view2 = self;
            } else {
                la = [self layoutAttributeForChar: *p];
                la2 = NSLayoutAttributeNotAnAttribute;
                view2 = nil;
            }
            s = p + 1;
            r = NSLayoutRelationEqual;
            if (*s == '>') {
                r = NSLayoutRelationGreaterThanOrEqual;
                s++;
            }
            else if (*s == '<') {
                r = NSLayoutRelationLessThanOrEqual;
                s++;
            }
            if (*s == '=') {
                s++;
            }
            while (*s && ((*s >= '0' && *s <= '9') || *s == '.')) {
                if (*s == '.') {
                    if (dec) {
                        NSLog( @"Bad format: %@", spec);
                        return @[];
                    }
                    dec = YES;
                }
                else if (dec) {
                    f = f / 10.0;
                    v = v + f * (*s - '0');
                }
                else if (!dec) {
                    v = 10.0 * v + (*s - '0');
                }
                s++;
            }
            if (*p == 'a') {
                //
                // Special case: aspect ratio
                //
                mult = v;
                v = 0.0;
            }
            cs = [NSLayoutConstraint constraintWithItem: self
                                              attribute: la
                                              relatedBy: r
                                                 toItem: view2
                                              attribute: la2
                                             multiplier: mult
                                               constant: v];
            [self addConstraint: cs];
            [constraints addObject: cs];
        }
    }
    return constraints;
}

- (void)addSubview: (UIView *)v pin: (NSString *)fmt, ...
{
    va_list args;
    va_start( args, fmt);
    NSString *spec = [[NSString alloc] initWithFormat: fmt arguments: args];
    va_end( args);
    [self addSubview: v];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wformat-nonliteral"
    [v pinToSuperview: spec];
#pragma clang diagnostic pop
}


@end