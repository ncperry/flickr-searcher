//
//  ViewController.h
//  Flickr Searcher
//
//  Created by Nate on 3/7/15.
//  Copyright (c) 2015 ncpApps. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    AlertViewSearching,
    AlertViewLoading,
} AlertViewType;

static NSString *flickrApiKey = @"602377835663528186747f68e006210d";
static NSInteger numSearchResults = 25;

@interface ViewController : UIViewController


@end

