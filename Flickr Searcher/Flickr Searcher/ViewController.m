//
//  ViewController.m
//  Flickr Searcher
//
//  Created by Nate on 3/7/15.
//  Copyright (c) 2015 ncpApps. All rights reserved.
//

#import "ViewController.h"
#import "UIView+Extensions.h"
#import "UINavigationController+Extensions.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>

@property (nonatomic, retain) NSOperationQueue *urlRequestQueue;

@property (nonatomic, retain) NSArray *photoTitles;
@property (nonatomic, retain) NSArray *photoSmallImageDatas;
@property (nonatomic, retain) NSArray *photoLargeImageURLs;

@property (nonatomic, retain) UITableView *tableview;

@property (nonatomic, retain) NSString *searchString;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.title = @"Flickr Searcher!";
    
    ///////////////////////
    // SET UP THE TABLEVIEW
    self.tableview = [UITableView new];
    self.tableview.translatesAutoresizingMaskIntoConstraints = NO;
    self.tableview.dataSource = self;
    self.tableview.delegate = self;
    
    // my coworker and I wrote our own autolayout api
    // this means set self.view.top = self.tableView.top, self.view.bottom = self.table.bottom, etc.
    [self.view addSubview:self.tableview pin:@"tt,bb,rr,ll"];
    
    ////////////////
    // SEARCH BUTTON
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"Search" style:UIBarButtonItemStylePlain target:self action:@selector(search)];
    self.navigationItem.rightBarButtonItem = anotherButton;
}



-(void)viewDidLayoutSubviews{
    // after presentation and rotation, our cells might not be sized correctly
    // so we can reload the tableView to easily resize them to make sure they don't get compressed
    [self.tableview reloadData];
}

#pragma mark - Setting Search String

-(void)search{
    UIAlertView *searchView = [[UIAlertView alloc] initWithTitle:@"Search Flickr"
                                                         message:nil
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                               otherButtonTitles:@"GO!", nil];
    searchView.alertViewStyle = UIAlertViewStylePlainTextInput;
    searchView.tag = AlertViewSearching;
    
    [searchView show];
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    switch (alertView.tag) {
        case AlertViewSearching:{
            if (buttonIndex == 1)
                [self makeFlickrRequestWithSearchString:[alertView textFieldAtIndex:0].text];
            break;
        }
        case AlertViewLoading:{
            // AlertViewLoading only has 1 button, but that button is at index 1
            // pressing that button should cancel
            // if makeFlickrRequesWithSearchString: calls dismissWithClickedButtonIndex:0, do nothing
            if (buttonIndex == 1)
                [self.urlRequestQueue cancelAllOperations];
            break;
        }
        default:
            break;
    }
    
    
    
}

#pragma mark - Making Requests to Flickr


-(NSOperationQueue *)urlRequestQueue{
    if (!_urlRequestQueue) {
        // by default runs on a background thread
        _urlRequestQueue = [[NSOperationQueue alloc] init];
    }
    
    return _urlRequestQueue;
}

-(void)makeFlickrRequestWithSearchString:(NSString*) searchText{
    
    /////////////////////
    // LOADING ALERT VIEW
    
    // show a loading alertView while we get the data from flickr
    UIAlertView *loadingView = [[UIAlertView alloc] initWithTitle:@"Fetching images"
                                                          message:nil
                                                         delegate:self
                                                cancelButtonTitle:nil
                                                otherButtonTitles:@"Cancel", nil];
    loadingView.tag = AlertViewLoading;

    UILabel *progressLabel = [UILabel new];
    progressLabel.textAlignment = NSTextAlignmentCenter;
    progressLabel.textColor = [UIColor blackColor];
    
    [loadingView setValue:progressLabel forKey:@"accessoryView"];
    [loadingView show];
    
    
    NSBlockOperation *requestBlock = [[NSBlockOperation alloc] init];
    __weak NSBlockOperation *weakReqeustBlock = requestBlock;
    [requestBlock addExecutionBlock:^{
        __strong NSBlockOperation *strongRequestBlock = weakReqeustBlock;
        
        // for when we need to get back on the main thread for UI stuff
        NSOperationQueue *mainQueue = [NSOperationQueue mainQueue];
        
        /////////////////////////////
        // SETUP AND MAKE THE REQUEST
        NSString *urlString = [NSString stringWithFormat:@"https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=%@&text=%@&per_page=%zd&format=json&nojsoncallback=1",flickrApiKey, searchText, numSearchResults];
        
        // so the user can use spaces between terms
        NSString *escapedString = (__bridge NSString*) CFURLCreateStringByAddingPercentEscapes(
                                                                  NULL,
                                                                  (CFStringRef)urlString,
                                                                  (CFStringRef)@"%+#",
                                                                  NULL,
                                                                  kCFStringEncodingUTF8);
        
        NSURL *flickrURL = [NSURL URLWithString:escapedString];
        
        NSURLRequest * urlRequest = [NSURLRequest requestWithURL:flickrURL];
        NSURLResponse * response = nil;
        NSError * error = nil;
        NSData * data = [NSURLConnection sendSynchronousRequest:urlRequest
                                              returningResponse:&response
                                                          error:&error];
        if (error) {
            NSString *errorMessage;
            switch (error.code) {
                case -1002: // unsupported URL, we probably parsed the search terms wrong somehow
                    errorMessage = @"We couldn't figure out what you were trying to search for. Try a new search?";
                    break;
                    
                case -1009:{
                    errorMessage = @"We couldn't find the server. Is your internet connected?";
                    break;
                }
                default:{
                    errorMessage = @"Something went wrong. Please try again";
                    break;
                }
                    
            }
            
            
            [mainQueue addOperation:[NSBlockOperation blockOperationWithBlock:^{
                [loadingView dismissWithClickedButtonIndex:0 animated:NO];
                UIAlertView *errorView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                message:errorMessage
                                                               delegate:nil
                                                      cancelButtonTitle:@"Ok"
                                                      otherButtonTitles: nil];
                [errorView show];
            }]];
            
            return;
        }
        
        NSLog(@"data: %@", data);
        
        ///////////////////////////////////////////
        // PARSE THE JSON AND SETUP OUR DATA SOURCE
        NSError *jsonError;
        id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
        NSArray *photos = [[json objectForKey:@"photos"] objectForKey:@"photo"];
        
        NSMutableArray *titles = [NSMutableArray arrayWithCapacity:photos.count];
        NSMutableArray *smallImageDatas = [NSMutableArray arrayWithCapacity:photos.count];
        NSMutableArray *largeImageURLs = [NSMutableArray arrayWithCapacity:photos.count];
        
        NSInteger progress = 0;
        
        for (NSDictionary *photo in photos) {
            
            NSString *title = [photo objectForKey:@"title"];
            [titles addObject:title];
            
            NSString *smallImageURLString =[NSString stringWithFormat:@"https://farm%@.static.flickr.com/%@/%@_%@_s.jpg", [photo objectForKey:@"farm"], [photo objectForKey:@"server"], [photo objectForKey:@"id"], [photo objectForKey:@"secret"]];
            NSData *smallImageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:smallImageURLString]];
            [smallImageDatas addObject:smallImageData];
            
            NSString *largeImageURLString = [NSString stringWithFormat:@"https://farm%@.static.flickr.com/%@/%@_%@_m.jpg",
                                             [photo objectForKey:@"farm"], [photo objectForKey:@"server"],
                                             [photo objectForKey:@"id"], [photo objectForKey:@"secret"]];
            [largeImageURLs addObject:[NSURL URLWithString:largeImageURLString]];
            
            // update the progress of the loading alert view
            ++progress;
            [mainQueue addOperation:[NSBlockOperation blockOperationWithBlock:^{
                progressLabel.text = [NSString stringWithFormat:@"%zd/%zd", progress, photos.count];
            }]];
        }

        
        //////////////////////////////////
        // RELOAD DATASOURCE AND TABLEVIEW
        
        // If the request was cancelled, don't bother reloading the dataSource and tableView
        if(!strongRequestBlock.cancelled){
        
            // only write to the data source on the main thread so that we never have two writes at the same time
            // reloadData obviously needs to be on the main thread
            [mainQueue addOperation:[NSBlockOperation blockOperationWithBlock:^{
                [loadingView dismissWithClickedButtonIndex:0 animated:YES];
                self.searchString = searchText;
                self.photoTitles = titles;
                self.photoSmallImageDatas = smallImageDatas;
                self.photoLargeImageURLs = largeImageURLs;
                [self.tableview reloadData];
            }]];
        }
    }];
    
    [self.urlRequestQueue addOperation:requestBlock];
    
    
}

#pragma mark - UITableView Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // 1 for the info cell
    // the other displays the thumbnails and titles
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0)
        return (self.searchString) ? 1 : 0; // if self.searchString has been set, display an info cell in section 0
    else
        return self.photoTitles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
        // often the titles that we get are pretty long, word wrap!
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    }
    
    if (indexPath.section == 0) {
        // Use section 0, to display a UITableView Cell showing what we are searching for
        
        // we'll use an attributed string to emphasize the search term
        NSString *titleText = @"Showing search results for: ";
        NSString *combinedString = [titleText stringByAppendingString:self.searchString];
        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:combinedString];
        [attString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:18] range:NSMakeRange(0, titleText.length)];
        [attString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:20] range:NSMakeRange(titleText.length, combinedString.length-titleText.length)];
        cell.textLabel.attributedText = attString;
        cell.imageView.image = nil;
    }else{
        // Cells in section 1 correspond to photos
        // will have a title and a thumbnail
        cell.textLabel.text = self.photoTitles[indexPath.row];
        NSData *smallImageData = self.photoSmallImageDatas[indexPath.row];
        cell.imageView.image = [UIImage imageWithData:smallImageData];
    }
    
    return cell;
}

#pragma mark - UITableView Delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ////////////////////////
    // IMAGE VIEW CONTROLLER
    UIViewController *largeImageViewController = [UIViewController new];
    largeImageViewController.view.backgroundColor = [UIColor lightGrayColor];
    largeImageViewController.title = self.photoTitles[indexPath.row];
    
    ////////////////
    // LOADING LABEL
    
    // we add a uilabel that just says loading
    // when the image loads, we'll add it to largeImageViewController.view, so it
    // will automaticatlly cover this up
    UILabel *loadingLable = [UILabel new];
    loadingLable.translatesAutoresizingMaskIntoConstraints = NO;
    loadingLable.text = @"Loading...";
    loadingLable.textColor = [UIColor blackColor];
    // this is using my coworker's and my autolayout api again
    // this one means largeImageViewController.view.centerX = loadingTable.centerX, largeImageViewController.view.centerY = loadingTable.centerY
    [largeImageViewController.view addSubview:loadingLable pin:@"cc,dd"];
    
    
    ///////////////
    // PRESENTATION
    [self.navigationController pushViewController:largeImageViewController animated:YES completion:^{
        
        // by hopping of of the main queue to load the image, we won't lock the back button
        // so the user doesn't have to wait for the image to load before exiting
        // GCD is sufficient here, instead of NSOperationQueue, because we don't have to do
        // anything with cancelling
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            /////////////////
            // LOAD THE IMAGE
            NSURL *largeImageURL = self.photoLargeImageURLs[indexPath.row];
            NSData *imageData = [NSData dataWithContentsOfURL:largeImageURL];
            
            // get back on the main thread to do UI work
            dispatch_async(dispatch_get_main_queue(), ^{
                
                ///////////////////////////////////////
                // CREATE THE IMAGEVIEW USING THE IMAGE
                UIImageView *largeImageView = [UIImageView new];
                largeImageView.contentMode = UIViewContentModeScaleAspectFit; // don't stretch, but fit to screen
                largeImageView.translatesAutoresizingMaskIntoConstraints = NO;
                [largeImageViewController.view addSubview:largeImageView pin:@"tt,bb,rr,ll"];
                UIImage *fullSizeImage = [UIImage imageWithData:imageData];
                largeImageView.image = fullSizeImage;
            });
        });
    }];
}



@end
