//
//  main.m
//  Flickr Searcher
//
//  Created by Nate on 3/7/15.
//  Copyright (c) 2015 ncpApps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
